package com.ianaydas.scheduler.queue;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;

@Configuration
public class RabbitMQMessagingConfig {
	
	public static final String EXCHANGE = "topic-scheduler-exchange";
	public static final String JOB_SCHEDULER_QUEUE = "JOB_SCHEDULER_QUEUE";
	
	@Bean
	public void declareExchange() throws IOException, TimeoutException {
		System.out.println("declare exchanges called..");
		Channel channel = ConnectionManager.getConnection().createChannel();
		channel.exchangeDeclare(EXCHANGE, BuiltinExchangeType.TOPIC,true);
		channel.close();
	}
	
	@Bean
	public void declareQueue() throws IOException, TimeoutException {
		System.out.println("declare queue called..");
		Channel channel = ConnectionManager.getConnection().createChannel();
		channel.queueDeclare(JOB_SCHEDULER_QUEUE, true, false, false, null);
		channel.close();
	}
	
	@Bean
	public void declareQueueBind() throws IOException, TimeoutException {
		System.out.println("bind queue called..");
		Channel channel = ConnectionManager.getConnection().createChannel();
		channel.queueBind(JOB_SCHEDULER_QUEUE, EXCHANGE, "scheduler.*");
		channel.close();
	}

}
