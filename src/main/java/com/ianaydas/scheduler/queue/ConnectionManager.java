package com.ianaydas.scheduler.queue;

import org.springframework.stereotype.Component;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

@Component
public class ConnectionManager {
	
	public static Connection connection = null;
	
	public static Connection getConnection() {
		
		Connection resConnection = connection;
		if(resConnection!=null) {
			return resConnection;
		}
		
		synchronized (ConnectionManager.class) {
			if(connection==null) {
				try {
					ConnectionFactory factory = new ConnectionFactory();
					connection = factory.newConnection();
					System.out.println("new rabbitmq connection created...");
				}catch (Exception e) {
					System.err.println("exception while creating rabbitmq connection...");
				}
			}
		}
		return connection;
	}

}
