package com.ianaydas.scheduler.service;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ianaydas.scheduler.dto.CreateJobRequestDTO;
import com.ianaydas.scheduler.dto.JobResponseDTO;
import com.ianaydas.scheduler.enums.JobType;
import com.ianaydas.scheduler.exception.InvalidJobException;
import com.ianaydas.scheduler.model.Job;
import com.ianaydas.scheduler.repository.JobRepository;
import com.ianaydas.scheduler.util.DateUtility;
import com.ianaydas.scheduler.util.TransformUtility;

@Service
public class JobService {

	@Autowired
	private JobRepository jobRepository;
	
	public String createJob(CreateJobRequestDTO requestDTO) throws InvalidJobException {
		try{
			Job job = new Job();
			job.setMaxRetry(requestDTO.getMax_retry());
			job.setInterval(requestDTO.getInterval());
			job.setTargetUrl(requestDTO.getTarget_url());
			job.setTargetUrlMethod(requestDTO.getTarget_url_method());
			job.setScheduled(Boolean.FALSE);
			try {
				job.setCreatedAt(DateUtility.getCurrentEpochTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			job.setStartTime(requestDTO.getStart_time());
			
			JobType jobType = JobType.valueOf(requestDTO.getType());
			job.setType(jobType.name());
			int s = 10/0;
			jobRepository.save(job);
		}catch (Exception e) {
			throw new InvalidJobException("failed to create job");
		}
		
		return "successfully created the job";
	}

	public List<JobResponseDTO> getAllJobs() {
		
		List<Job> jobs = jobRepository.findAll();
		List<JobResponseDTO> dtos = TransformUtility.listOfJobsToResponseDTOs.apply(jobs);
		return dtos;
	}
	
	
}
