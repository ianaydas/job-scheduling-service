package com.ianaydas.scheduler.task;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.ianaydas.scheduler.dto.PublishJobDTO;
import com.ianaydas.scheduler.enums.SchedulerStatus;
import com.ianaydas.scheduler.model.JobHistory;
import com.ianaydas.scheduler.producer.ScheduledJobProducer;
import com.ianaydas.scheduler.repository.JobHistoryRepository;
import com.ianaydas.scheduler.util.DateUtility;

@Component
public class RetryTask {

	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	
	@Autowired
	private ScheduledJobProducer producer;
	
	@Bean
	public void runRetryTask() {
		
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				List<JobHistory> histories = jobHistoryRepository.findAllFailedJobHistory();
				List<PublishJobDTO> publishJobs = new ArrayList<>();
				List<JobHistory> updatingJobHistories = new ArrayList<>();
				
				histories.stream().filter(history-> history.getRetryCount()<history.getJob().getMaxRetry()).forEach(history->{
					
					//JobHistory history = new JobHistory();
					
					history.setStatus(SchedulerStatus.STARTED.name());
					int updatedRetryCount = history.getRetryCount()+1;
					history.setRetryCount(updatedRetryCount);
					try {
						history.setExecutionTime(DateUtility.getCurrentEpochTimeInMillis());
					} catch (ParseException e) {
						e.printStackTrace();
					}
					updatingJobHistories.add(history);
					
					
					PublishJobDTO publishDTO = new PublishJobDTO();
					publishDTO.setJob_id(history.getJob().getJobId());
					publishDTO.setJob_history_id(history.getJobHistoryId());
					publishDTO.setExecution_time(history.getExecutionTime());
					publishDTO.setRetry_count(history.getRetryCount());
					publishDTO.setStatus(history.getStatus());
					publishJobs.add(publishDTO);
				});
				
				if(!updatingJobHistories.isEmpty()) {
					jobHistoryRepository.saveAll(updatingJobHistories);
					publishJobs.stream().forEach(pj->{
						try {
							producer.publishJob(pj);
						} catch (IOException | TimeoutException e) {
							e.printStackTrace();
						}
					});
				}
			}
		};
		
		Timer timer = new Timer(true);
		timer.schedule(task, 10000, 6000);
		try {
			Thread.sleep(6000);
		} catch (Exception e) {
		
		}
	}
}
