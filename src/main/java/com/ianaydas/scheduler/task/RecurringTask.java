package com.ianaydas.scheduler.task;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import org.apache.tomcat.jni.Time;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ianaydas.scheduler.dto.PublishJobDTO;
import com.ianaydas.scheduler.enums.JobType;
import com.ianaydas.scheduler.enums.SchedulerStatus;
import com.ianaydas.scheduler.model.JobHistory;
import com.ianaydas.scheduler.model.JobSchedule;
import com.ianaydas.scheduler.producer.ScheduledJobProducer;
import com.ianaydas.scheduler.repository.JobHistoryRepository;
import com.ianaydas.scheduler.repository.JobScheduleRepository;
import com.ianaydas.scheduler.util.DateUtility;

@Configuration
public class RecurringTask {

	
	@Autowired
	private JobScheduleRepository jobScheduleRepository;
	
	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	
	@Autowired
	private ScheduledJobProducer producer;
	
	@Bean
	public void runRecurringTask() {
		
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				
				try {
					System.out.println("recuuring task running....");
					Long currentTime = DateUtility.getCurrentEpochMinute();
					
					List<JobSchedule> schedlues = jobScheduleRepository.findAllScheduledJobs(currentTime);
					
					List<JobHistory> histories = new ArrayList<>();
					
					List<PublishJobDTO> publishReqDTOs  = new ArrayList<>();
					List<JobSchedule> updatingSchedules = new ArrayList<>();
					
					schedlues.stream().filter(j-> JobType.RECURRING.name().equalsIgnoreCase(j.getJob().getType())).forEach(sch->{
						
						sch.setNextExecutionTime(DateUtility.getNextExecution(sch.getJob().getInterval()));
						updatingSchedules.add(sch);
						
						
						JobHistory history = new JobHistory();
						try {
							history.setExecutionTime(DateUtility.getCurrentEpochTimeInMillis());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						history.setJob(sch.getJob());
						history.setRetryCount(0);
						history.setStatus(SchedulerStatus.STARTED.name());
						histories.add(history);
						
						PublishJobDTO publishJobDTO = new PublishJobDTO();
						publishJobDTO.setJob_id(history.getJob().getJobId());
						publishJobDTO.setJob_history_id(history.getJobHistoryId());
						publishJobDTO.setRetry_count(history.getRetryCount());
						publishJobDTO.setExecution_time(history.getExecutionTime());
						publishReqDTOs.add(publishJobDTO);
						
					});

					if(!histories.isEmpty()) {
						jobHistoryRepository.saveAll(histories);
						jobScheduleRepository.saveAll(updatingSchedules);
						publishReqDTOs.stream().forEach(pub->{
							try {
								producer.publishJob(pub);
							} catch (IOException | TimeoutException e) {
								System.out.println(String.format("unable to publish recurring job with id %s to queuw", pub.getJob_id()));
								e.printStackTrace();
							}
						});
					}
					
				}catch (Exception e) {
					e.printStackTrace();
				}
			}
		};
		
		Timer timer = new Timer(true);
		timer.schedule(task, 10000, 6000);
		try {
			Thread.sleep(6000);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
