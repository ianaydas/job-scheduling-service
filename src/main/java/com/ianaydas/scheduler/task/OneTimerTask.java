package com.ianaydas.scheduler.task;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeoutException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ianaydas.scheduler.dto.PublishJobDTO;
import com.ianaydas.scheduler.enums.JobType;
import com.ianaydas.scheduler.enums.SchedulerStatus;
import com.ianaydas.scheduler.model.JobHistory;
import com.ianaydas.scheduler.model.JobSchedule;
import com.ianaydas.scheduler.producer.ScheduledJobProducer;
import com.ianaydas.scheduler.repository.JobHistoryRepository;
import com.ianaydas.scheduler.repository.JobRepository;
import com.ianaydas.scheduler.repository.JobScheduleRepository;
import com.ianaydas.scheduler.util.DateUtility;

@Configuration
public class OneTimerTask {

	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private JobScheduleRepository jobScheduleRepository;
	
	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	
	@Autowired
	private ScheduledJobProducer producer;
	
	@Bean
	public void runOneTimerTask() {
		
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				
				System.out.println("One timer task is running..");
				try {
					Long currentTime = DateUtility.getCurrentEpochMinute();
					
					List<JobSchedule> schedlues = jobScheduleRepository.findAllScheduledJobs(currentTime);
					
					List<JobHistory> histories = new ArrayList<>();
					
					List<PublishJobDTO> publishReqDTOs  = new ArrayList<>();
					List<JobSchedule> deletingSchedule = new ArrayList<>();
					schedlues.stream().filter(schedule->JobType.ONE_TIME.toString().equalsIgnoreCase(schedule.getJob().getType())).forEach(schedule->{
						
						JobHistory history = new JobHistory();
						history.setJob(schedule.getJob());
						try {
							history.setExecutionTime(DateUtility.getCurrentEpochTimeInMillis());
						} catch (ParseException e) {
							e.printStackTrace();
						}
						history.setStatus(SchedulerStatus.STARTED.name());
						history.setRetryCount(0);
						histories.add(history);
						
						PublishJobDTO publishJobDTO = new PublishJobDTO();
						publishJobDTO.setJob_id(history.getJob().getJobId());
						publishJobDTO.setJob_history_id(history.getJobHistoryId());
						publishJobDTO.setRetry_count(history.getRetryCount());
						publishJobDTO.setExecution_time(history.getExecutionTime());
						publishReqDTOs.add(publishJobDTO);
						
						deletingSchedule.add(schedule);
						
					});
					
					if(!histories.isEmpty()) {
						jobHistoryRepository.saveAll(histories);
						publishReqDTOs.stream().forEach(pj->{
							try {
								producer.publishJob(pj);
							} catch (IOException | TimeoutException e) {
								e.printStackTrace();
							}
						});
					}
					
					if(!deletingSchedule.isEmpty()) {
						jobScheduleRepository.deleteAllInBatch(deletingSchedule);
					}
					
				} catch (ParseException e) {
					e.printStackTrace();
				}
				
				
				
			}
		};
		

		Timer timer = new Timer(true);
		timer.schedule(task, 10000, 6000);
		
		try {
			Thread.sleep(60000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	
}
