package com.ianaydas.scheduler.task;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ianaydas.scheduler.enums.SchedulerStatus;
import com.ianaydas.scheduler.model.Job;
import com.ianaydas.scheduler.model.JobHistory;
import com.ianaydas.scheduler.model.JobSchedule;
import com.ianaydas.scheduler.repository.JobHistoryRepository;
import com.ianaydas.scheduler.repository.JobRepository;
import com.ianaydas.scheduler.repository.JobScheduleRepository;
import com.ianaydas.scheduler.util.DateUtility;

@Configuration
public class SchedulingTask {

	@Autowired
	private JobRepository jobRepository;
	
	@Autowired
	private JobScheduleRepository jobScheduleRepository;
	
	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	
	@Bean
	public void runTask() {
		TimerTask task = new TimerTask() {
			
			@Override
			public void run() {
				System.out.println("scheduling task running...");
				try {
					List<Job> unScheduledJobs = jobRepository.findAllUnScheduledJobs();
					List<JobSchedule> schedulingJobs = new ArrayList<>();
					List<JobHistory> histories = new ArrayList<>();
					List<Job> updatingJobs = new ArrayList<>();
					
					unScheduledJobs.stream().forEach(usj->{
						JobSchedule schedule = new JobSchedule();
						schedule.setNextExecutionTime(DateUtility.getNextExecution(usj.getInterval()));
						schedule.setJob(usj);
						schedulingJobs.add(schedule);
						
						usj.setScheduled(Boolean.TRUE);
						updatingJobs.add(usj);
						
						try {
							JobHistory history = new JobHistory();
							history.setJob(usj);
							history.setRetryCount(0);
							history.setStatus(SchedulerStatus.SCHEDULED.toString());
							history.setExecutionTime(DateUtility.getCurrentEpochTimeInMillis());
							histories.add(history);
						} catch (ParseException e) {
							e.printStackTrace();
						}
					});
					if(!schedulingJobs.isEmpty()) {
						jobScheduleRepository.saveAll(schedulingJobs);
						jobHistoryRepository.saveAll(histories);
						jobRepository.saveAll(updatingJobs);
						System.out.println("scheduling jobs...");
					}
				}catch (Exception e) {
					e.printStackTrace();
					System.out.println("exception during the scheduling jobs...");
				}
				
			}
		};
		
		Timer timer = new Timer(true);
		timer.schedule(task, 10000, 6000);
		
		try {
	        Thread.sleep(60000); // Cancel task after 1 minute.
	    } catch (InterruptedException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }
	}
	
}
