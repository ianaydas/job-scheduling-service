package com.ianaydas.scheduler.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobHistoryResponseDTO {
	
	@JsonProperty(value = "execution_time")
	public Long executionTime;
	
	public String status;
	
	@JsonProperty(value = "retry_count")
	public Integer retryCount;
	
	@JsonProperty(value = "history_id")
	public String historyId;

	public JobHistoryResponseDTO() {
		super();
	}

	public Long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public String getHistoryId() {
		return historyId;
	}

	public void setHistoryId(String historyId) {
		this.historyId = historyId;
	}

}
