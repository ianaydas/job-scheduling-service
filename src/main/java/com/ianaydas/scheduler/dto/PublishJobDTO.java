package com.ianaydas.scheduler.dto;

public class PublishJobDTO {
	
	private String job_id;
	
	private String status;
	
	private Integer retry_count;
	
	private Long execution_time;
	
	private String job_history_id;

	public PublishJobDTO() {
		super();
	}

	public String getJob_id() {
		return job_id;
	}

	public void setJob_id(String job_id) {
		this.job_id = job_id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRetry_count() {
		return retry_count;
	}

	public void setRetry_count(Integer retry_count) {
		this.retry_count = retry_count;
	}

	public Long getExecution_time() {
		return execution_time;
	}

	public void setExecution_time(Long execution_time) {
		this.execution_time = execution_time;
	}

	public String getJob_history_id() {
		return job_history_id;
	}

	public void setJob_history_id(String job_history_id) {
		this.job_history_id = job_history_id;
	}

}
