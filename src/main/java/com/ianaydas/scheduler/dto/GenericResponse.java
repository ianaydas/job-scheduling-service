package com.ianaydas.scheduler.dto;


import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.ianaydas.scheduler.enums.ResponseStatus;
import com.ianaydas.scheduler.exception.CustomeError;

public class GenericResponse<T> {

	private ResponseStatus status;
	
	private CustomeError error;
	
	@JsonProperty(value = "status_code")
	private HttpStatus statusCode;
	
	T data;

	
	
	public GenericResponse(ResponseStatus status, CustomeError error, HttpStatus statusCode, T data) {
		super();
		this.status = status;
		this.error = error;
		this.statusCode = statusCode;
		this.data = data;
	}

	public GenericResponse(ResponseStatus status) {
		super();
		this.status = status;
	}

	public GenericResponse(CustomeError error) {
		super();
		this.error = error;
		this.status = ResponseStatus.FAILURE;
	}

	public GenericResponse(T data) {
		super();
		this.data = data;
		this.status = ResponseStatus.SUCCESS;
	}

	public ResponseStatus getStatus() {
		return status;
	}

	public void setStatus(ResponseStatus status) {
		this.status = status;
	}

	public CustomeError getError() {
		return error;
	}

	public void setError(CustomeError error) {
		this.error = error;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(HttpStatus statusCode) {
		this.statusCode = statusCode;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
	
}
