package com.ianaydas.scheduler.dto;

public class CreateJobRequestDTO {
	
	public String name;
	
	public Long max_retry;
	
	public String interval;
	
	public String type;
	
	public String target_url;
	
	public String target_url_method;
	
	public Long start_time;

	public CreateJobRequestDTO() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getMax_retry() {
		return max_retry;
	}

	public void setMax_retry(Long max_retry) {
		this.max_retry = max_retry;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTarget_url() {
		return target_url;
	}

	public void setTarget_url(String target_url) {
		this.target_url = target_url;
	}

	public String getTarget_url_method() {
		return target_url_method;
	}

	public void setTarget_url_method(String target_url_method) {
		this.target_url_method = target_url_method;
	}

	public Long getStart_time() {
		return start_time;
	}

	public void setStart_time(Long start_time) {
		this.start_time = start_time;
	}
	
	
}
