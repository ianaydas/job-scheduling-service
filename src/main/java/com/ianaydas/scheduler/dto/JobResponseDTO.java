package com.ianaydas.scheduler.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JobResponseDTO {
	
	@JsonProperty(value = "job_id")
	private  String jobId;
	
	private String name;
	
	private String type;
	
	@JsonProperty(value = "next_execution_time")
	private Long nextExecutionTime;
	
	@JsonProperty(value = "max_retry_count")
	private Long maxRetryCount;
	
	private String interval;
	
	private List<JobHistoryResponseDTO> histories;
	
	@JsonProperty(value = "is_scheduled")
	private Boolean isScheduled;

	public JobResponseDTO() {
		super();
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Long getNextExecutionTime() {
		return nextExecutionTime;
	}

	public void setNextExecutionTime(Long nextExecutionTime) {
		this.nextExecutionTime = nextExecutionTime;
	}

	public Long getMaxRetryCount() {
		return maxRetryCount;
	}

	public void setMaxRetryCount(Long maxRetryCount) {
		this.maxRetryCount = maxRetryCount;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public List<JobHistoryResponseDTO> getHistories() {
		return histories;
	}

	public void setHistories(List<JobHistoryResponseDTO> histories) {
		this.histories = histories;
	}

	public Boolean getIsScheduled() {
		return isScheduled;
	}

	public void setIsScheduled(Boolean isScheduled) {
		this.isScheduled = isScheduled;
	}
	
}
