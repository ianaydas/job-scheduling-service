package com.ianaydas.scheduler.producer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.ianaydas.scheduler.dto.PublishJobDTO;
import com.ianaydas.scheduler.queue.ConnectionManager;
import com.ianaydas.scheduler.queue.RabbitMQMessagingConfig;
import com.rabbitmq.client.Channel;

@Component
public class ScheduledJobProducer {
	
	@Autowired
	private RabbitTemplate template;
	
	public void publishJob(PublishJobDTO jobHistory) throws IOException, TimeoutException {
		Channel channel = ConnectionManager.getConnection().createChannel();
		Gson gson = new Gson();
		String jobHistoryJson = gson.toJson(jobHistory, PublishJobDTO.class);
		System.out.println("publishing job to queue by producer...");
		channel.basicPublish(RabbitMQMessagingConfig.EXCHANGE, "scheduler.job", null, jobHistoryJson.getBytes());
		channel.close();
		
		//template.convertAndSend(RabbitMQMessagingConfig.EXCHANGE, RabbitMQMessagingConfig.JOB_SCHEDULER_QUEUE, jobHistoryJson);
	}

}
