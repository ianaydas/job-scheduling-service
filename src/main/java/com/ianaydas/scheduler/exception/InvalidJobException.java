package com.ianaydas.scheduler.exception;

public class InvalidJobException extends Exception{
	
	private CustomeError error;

	public InvalidJobException(CustomeError error) {
		super();
		this.error = error;
	}

	public InvalidJobException(String message) {
		super(message);
	}
	
	public InvalidJobException(CustomeError error,String message) {
		super(message);
		this.error = error;
	}
	
	public InvalidJobException(CustomeError error,Throwable cause) {
		super(cause);
		this.error = error;
	}
	
	public InvalidJobException(CustomeError error,String message,Throwable cause) {
		super(message,cause);
		this.error = error;
	}

}
