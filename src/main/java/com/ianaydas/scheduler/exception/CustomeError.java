package com.ianaydas.scheduler.exception;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CustomeError {
	
	@JsonProperty(value = "error_code")
	private String errorCode;
	
	@JsonProperty(value = "developer_message")
	private String developerMessage;
	
	@JsonProperty(value = "status_code")
	private HttpStatus statusCode;
	
	@JsonProperty(value = "custom_message")
	private String customMessage;

	
	public CustomeError() {
		super();
	}

	public CustomeError(String errorCode, HttpStatus statusCode, String customMessage) {
		super();
		this.errorCode = errorCode;
		this.statusCode = statusCode;
		this.customMessage = customMessage;
	}

	public CustomeError(String errorCode, String developerMessage, HttpStatus statusCode, String customMessage) {
		super();
		this.errorCode = errorCode;
		this.developerMessage = developerMessage;
		this.statusCode = statusCode;
		this.customMessage = customMessage;
	}

	public String getErrorCode() {
		return errorCode;
	}



	public String getDeveloperMessage() {
		return developerMessage;
	}



	public HttpStatus getStatusCode() {
		return statusCode;
	}



	public String getCustomMessage() {
		return customMessage;
	}

	
	

}
