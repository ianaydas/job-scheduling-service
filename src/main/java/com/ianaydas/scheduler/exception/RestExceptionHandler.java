package com.ianaydas.scheduler.exception;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ianaydas.scheduler.dto.GenericResponse;
import com.ianaydas.scheduler.enums.ResponseStatus;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler({InvalidJobException.class})
	public ResponseEntity<Object> handleInvalidJobException(final InvalidJobException e,WebRequest request){
		String developerMsg = "unable to run job";
		return createResponseEntityFromException(e.getMessage(), "400", e, HttpStatus.INTERNAL_SERVER_ERROR, "");
	}
	
	private ResponseEntity<Object> createResponseEntityFromException(String message,String errorCode
			,Exception ex, HttpStatus httpStatus,String developerMessage){
	
		CustomeError error = new CustomeError(errorCode, developerMessage, httpStatus, message);
		GenericResponse<?> response = new GenericResponse(ResponseStatus.FAILURE, error, httpStatus, null);
		return new ResponseEntity<>(response,new HttpHeaders(),httpStatus);
	}

}
