package com.ianaydas.scheduler.model;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "job_history")
@Entity
public class JobHistory {

	@Id
	@Column(name = "job_history_id")
	public String jobHistoryId = UUID.randomUUID().toString();
	
	@Column(name = "execution_time")
	public Long executionTime;
	
	public String status;
	
	@Column(name = "retry_count")
	public Integer retryCount;
	
	@ManyToOne(fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name = "job_id",referencedColumnName = "job_id",nullable = false)
	public Job job;

	public JobHistory() {
		super();
	}

	public String getJobHistoryId() {
		return jobHistoryId;
	}

	public void setJobHistoryId(String jobHistoryId) {
		this.jobHistoryId = jobHistoryId;
	}

	public Long getExecutionTime() {
		return executionTime;
	}

	public void setExecutionTime(Long executionTime) {
		this.executionTime = executionTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Integer getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(Integer retryCount) {
		this.retryCount = retryCount;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
}
