package com.ianaydas.scheduler.model;

import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Table(name = "job")
@Entity
public class Job {

	@Id
	@Column(name = "job_id")
	private String jobId = UUID.randomUUID().toString();
	
	private String name;
	
	@Column(name = "max_retry")
	private Long maxRetry;
	
	@Column(name = "run_interval")
	private String interval;
	
	private String type;
	
	@Column(name = "target_url")
	private String targetUrl;
	
	@Column(name = "target_url_method")
	private String targetUrlMethod;
	
	@Column(name = "is_scheduled")
	private boolean isScheduled;
	
	@Column(name = "created_by")
	private String createdBy;
	
	@Column(name = "created_at")
	public Long createdAt;
	
	@Column(name = "start_time")
	public Long startTime;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "job")
	private List<JobSchedule> schedules;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "job")
	private List<JobHistory> histories;

	public Job() {
		super();
	}

	public String getJobId() {
		return jobId;
	}

	public void setJobId(String jobId) {
		this.jobId = jobId;
	}

	public Long getMaxRetry() {
		return maxRetry;
	}

	public void setMaxRetry(Long maxRetry) {
		this.maxRetry = maxRetry;
	}

	public String getInterval() {
		return interval;
	}

	public void setInterval(String interval) {
		this.interval = interval;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getTargetUrl() {
		return targetUrl;
	}

	public void setTargetUrl(String targetUrl) {
		this.targetUrl = targetUrl;
	}

	public String getTargetUrlMethod() {
		return targetUrlMethod;
	}

	public void setTargetUrlMethod(String targetUrlMethod) {
		this.targetUrlMethod = targetUrlMethod;
	}


	public void setScheduled(boolean isScheduled) {
		this.isScheduled = isScheduled;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Long createdAt) {
		this.createdAt = createdAt;
	}

	public List<JobSchedule> getSchedules() {
		return schedules;
	}

	public void setSchedules(List<JobSchedule> schedules) {
		this.schedules = schedules;
	}

	public List<JobHistory> getHistories() {
		return histories;
	}

	public void setHistories(List<JobHistory> histories) {
		this.histories = histories;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public boolean isScheduled() {
		return isScheduled;
	}
	
}
