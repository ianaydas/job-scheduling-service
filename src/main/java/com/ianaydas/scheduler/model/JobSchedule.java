package com.ianaydas.scheduler.model;

import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table(name = "job_schedule")
@Entity
public class JobSchedule {

	@Id
	@Column(name = "job_schedule_id")
	public String jobSchdeuleId = UUID.randomUUID().toString();
	
	@Column(name = "next_execution_time")
	public Long nextExecutionTime;
	
	@ManyToOne(fetch = FetchType.EAGER,optional = false)
	@JoinColumn(name = "job_id",referencedColumnName = "job_id",nullable = false)
	public Job job;

	public JobSchedule() {
		super();
	}

	public String getJobSchdeuleId() {
		return jobSchdeuleId;
	}

	public void setJobSchdeuleId(String jobSchdeuleId) {
		this.jobSchdeuleId = jobSchdeuleId;
	}

	public Long getNextExecutionTime() {
		return nextExecutionTime;
	}

	public void setNextExecutionTime(Long nextExecutionTime) {
		this.nextExecutionTime = nextExecutionTime;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}
	
}
