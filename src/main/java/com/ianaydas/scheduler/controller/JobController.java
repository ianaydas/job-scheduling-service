package com.ianaydas.scheduler.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ianaydas.scheduler.dto.CreateJobRequestDTO;
import com.ianaydas.scheduler.dto.GenericResponse;
import com.ianaydas.scheduler.dto.JobResponseDTO;
import com.ianaydas.scheduler.exception.InvalidJobException;
import com.ianaydas.scheduler.service.JobService;

@RestController
@RequestMapping("/scheduler")
public class JobController {
	
	@Autowired
	private JobService jobService;
	
	@RequestMapping(value = "/job/create",method = RequestMethod.POST)
	public ResponseEntity<Object> createJob(@RequestBody CreateJobRequestDTO requestDTO) throws InvalidJobException{
		String message = jobService.createJob(requestDTO);
		GenericResponse<?> response = new GenericResponse(message);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/jobs",method = RequestMethod.GET)
	public ResponseEntity<Object> getJobList() throws InvalidJobException{
		List<JobResponseDTO> jobs = jobService.getAllJobs();
		GenericResponse<?> response = new GenericResponse(jobs);
		return new ResponseEntity<Object>(response, HttpStatus.OK);
	}
}
