package com.ianaydas.scheduler.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ianaydas.scheduler.model.Job;

@Repository
public interface JobRepository extends JpaRepository<Job, String>{

	@Query(nativeQuery = true, value = "select * from job where is_scheduled=0")
	List<Job> findAllUnScheduledJobs();
	
}
