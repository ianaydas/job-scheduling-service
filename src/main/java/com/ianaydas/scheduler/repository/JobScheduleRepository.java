package com.ianaydas.scheduler.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ianaydas.scheduler.model.JobSchedule;

@Repository
public interface JobScheduleRepository extends JpaRepository<JobSchedule, String>{

	@Query(nativeQuery = true,value = "select * from job_schedule where next_execution_time=:nextExecutionTime")
	List<JobSchedule> findAllScheduledJobs(Long nextExecutionTime);
	
}
