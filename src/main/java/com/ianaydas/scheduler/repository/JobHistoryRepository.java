package com.ianaydas.scheduler.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ianaydas.scheduler.model.JobHistory;

@Repository
public interface JobHistoryRepository extends JpaRepository<JobHistory, String>{
	
	@Query(nativeQuery = true, value = "select * from job_history where status ='FAILED'")
	List<JobHistory> findAllFailedJobHistory();

}
