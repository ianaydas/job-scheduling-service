package com.ianaydas.scheduler.enums;

public enum SchedulerStatus {
	SCHEDULED,STARTED,PROGRESS,COMPLETED,FAILED
}
