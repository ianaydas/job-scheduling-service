package com.ianaydas.scheduler.util;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import com.ianaydas.scheduler.dto.JobHistoryResponseDTO;
import com.ianaydas.scheduler.dto.JobResponseDTO;
import com.ianaydas.scheduler.model.Job;

public class TransformUtility {
	
	public static Function<List<Job>, List<JobResponseDTO>> listOfJobsToResponseDTOs = (jobs)->{
		
		List<JobResponseDTO> jobResponseDTOs = new ArrayList();
		jobs.stream().forEach(job->{
			JobResponseDTO jobResponseDTO = new JobResponseDTO();
			jobResponseDTO.setJobId(job.getJobId());
			
			jobResponseDTO.setInterval(job.getInterval());
			jobResponseDTO.setIsScheduled(job.isScheduled());
			jobResponseDTO.setMaxRetryCount(job.getMaxRetry());
			jobResponseDTO.setName(job.getName());
			jobResponseDTO.setNextExecutionTime(job.getSchedules()==null || job.getSchedules().isEmpty() ? null: job.getSchedules().get(0).getNextExecutionTime());
			jobResponseDTO.setType(job.getType());
			
			List<JobHistoryResponseDTO> historyDTOs = new ArrayList();
			job.getHistories().stream().forEach(history->{
				JobHistoryResponseDTO historyDTO = new JobHistoryResponseDTO();
				historyDTO.setHistoryId(history.getJobHistoryId());
				historyDTO.setExecutionTime(history.getExecutionTime());
				historyDTO.setRetryCount(history.getRetryCount());
				historyDTO.setStatus(history.getStatus());
				historyDTOs.add(historyDTO);
			});
			jobResponseDTO.setHistories(historyDTOs);
			jobResponseDTOs.add(jobResponseDTO);
		});
		
		
		
		return jobResponseDTOs;
	};

}
