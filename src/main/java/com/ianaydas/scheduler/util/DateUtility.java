package com.ianaydas.scheduler.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

public class DateUtility {
	
	private static final long ONE_MINUTE_EPOCH = 60L;
	
	/*public static Long getCurrentEpochTime() throws ParseException {
		Date currentDate = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = dateFormat.format(currentDate);
		return dateFormat.parse(dateString).getTime()/1000;
	}*/
	
	public static Long getCurrentEpochTime() throws ParseException {
		
		return LocalDateTime.now().atZone(ZoneOffset.systemDefault())
				.toInstant().toEpochMilli()/1000;
	}
	
	public static Long getCurrentEpochTimeInMillis() throws ParseException {
		
		return LocalDateTime.now().atZone(ZoneOffset.systemDefault())
				.toInstant().toEpochMilli();
	}
	
	public static Long getCurrentEpochMinute() throws ParseException {
		return LocalDateTime.now().atZone(ZoneOffset.systemDefault())
				.toInstant().getEpochSecond()/60;
	}
	
	public static Long getNextExecution(CharSequence executionInterval) {
		return LocalDateTime.now()
							.plus(Duration.parse(executionInterval))
							.atZone(ZoneOffset.systemDefault())
							.toInstant()
							.getEpochSecond()/60;
	}
	
	public static Long getTimeFromMilliToMinutes(Long epochMili) {
		return epochMili/60000;
	}

}
