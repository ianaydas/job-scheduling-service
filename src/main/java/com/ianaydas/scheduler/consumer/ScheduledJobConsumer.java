package com.ianaydas.scheduler.consumer;

import java.io.IOException;
import java.text.ParseException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.ianaydas.scheduler.dto.PublishJobDTO;
import com.ianaydas.scheduler.enums.SchedulerStatus;
import com.ianaydas.scheduler.model.JobHistory;
import com.ianaydas.scheduler.queue.ConnectionManager;
import com.ianaydas.scheduler.queue.RabbitMQMessagingConfig;
import com.ianaydas.scheduler.repository.JobHistoryRepository;
import com.ianaydas.scheduler.util.DateUtility;
import com.rabbitmq.client.Channel;

@Component
public class ScheduledJobConsumer {

	@Autowired
	private JobHistoryRepository jobHistoryRepository;
	
	@Bean
	public void ScheduledJobConsumer() {
		
		try {
			Channel channel = ConnectionManager.getConnection().createChannel();
			channel.basicConsume(RabbitMQMessagingConfig.JOB_SCHEDULER_QUEUE, true, ((consumerTag,message)->{
			Gson gson = new Gson();
			PublishJobDTO historyDTO = gson.fromJson(new String(message.getBody()), PublishJobDTO.class);
			 System.out.println("consuming from queue with job id: "+historyDTO.getJob_id());
			 
			 Optional<JobHistory> historyOptional = jobHistoryRepository.findById(historyDTO.getJob_history_id());
			 if(historyOptional.isPresent()) {
				 JobHistory history = historyOptional.get();
				 JobHistory newHistory = new JobHistory();
				 newHistory.setJob(history.getJob());
				 newHistory.setRetryCount(history.getRetryCount());
				 newHistory.setStatus(SchedulerStatus.PROGRESS.name());
				 //newHistory.setStatus(SchedulerStatus.FAILED.name());
				 try {
					newHistory.setExecutionTime(DateUtility.getCurrentEpochTimeInMillis());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				jobHistoryRepository.save(newHistory);
			 }
				
			}), (consumerTag)->{System.out.println(consumerTag);});
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
}
